 PDF To Markdown Converter
Debug View
Result View
# Programming Languages (Coursera / University of Washington)

# Assignment 2


You will write 11 SML functions (not counting local helper functions), 4 related to “name substitutions”
and 7 related to a made-up solitaire card game.
Your solutions must use pattern-matching. You may not use the functionsnull,hd,tl,isSome, orvalOf,
nor may you use anything containing a#character or features not used in class (such as mutation). Note
that list order does not matter unless specifically stated in the problem.
Download hw2provided.sml from the course website.The provided code defines several types for you. You
will not need to add any additionaldatatypebindings or type synonyms.
The sample solution, not including challenge problems, isroughly130 lines, including the provided code.
Do not miss the “Important Caveat” and “Assessment” after the “Type Summary.”

1. This problem involves using first-name substitutions to come up with alternate names. For example,
Fredrick William Smithcould also beFred William Smith orFreddie William Smith. Only part (d) is
specifically about this, but the other problems are helpful.


(a) Write a functionall_except_option, which takes astringand astring list. ReturnNONEif the
string is not in the list, else returnSOMElstwherelstis identical to the argument list except the string
is not in it. You may assume the string is in the list at most once. Usesame_string, provided to you,
to compare strings. Sample solution is around 8 lines.

(b) Write a functionget_substitutions1, which takes astring list list(a list of list of strings, the
substitutions) and astringsand returns astring list. The result has all the strings that are in
some list insubstitutionsthat also hass, butsitself should not be in the result. Example:
```
get_substitutions1([["Fred","Fredrick"],["Elizabeth","Betty"],["Freddie","Fred","F"]],
"Fred")
(* answer: ["Fredrick","Freddie","F"] *)
Assume each list insubstitutionshas no repeats. The result will have repeats ifsand another string are
both in more than one list insubstitutions. Example:
get_substitutions1([["Fred","Fredrick"],["Jeff","Jeffrey"],["Geoff","Jeff","Jeffrey"]],
"Jeff")
(* answer: ["Jeffrey","Geoff","Jeffrey"] *)

```
Use part (a) and ML’s list-append (@) but no other helper functions. Sample solution is around 6 lines.
(c) Write a functionget_substitutions2, which is likeget_substitutions1except it uses a tail-recursive
local helper function.

(d) Write a functionsimilar_names, which takes astring list listof substitutions (as in parts (b) and
(c)) and afull nameof type{first:string,middle:string,last:string}and returns a list of full
names (type{first:string,middle:string,last:string} list). The result is all the full names you
can produce by substituting for the first name (andonly the first name) usingsubstitutionsand parts (b)
or (c). The answer should begin with the original name (then have 0 or more other names). Example:
```
similar_names([["Fred","Fredrick"],["Elizabeth","Betty"],["Freddie","Fred","F"]],
{first="Fred", middle="W", last="Smith"})
(* answer: [{first="Fred", last="Smith", middle="W"},
{first="Fredrick", last="Smith", middle="W"},
{first="Freddie", last="Smith", middle="W"},
{first="F", last="Smith", middle="W"}] *)

```
Do not eliminate duplicates from the answer. Hint: Use a local helper function. Sample solution is
around 10 lines.


2. This problem involves a solitaire card game invented just for this question. You will write a program that
tracks the progress of a game; writing a game player is a challenge problem. You can do parts (a)–(e) before
understanding the game if you wish.
A game is played with acard-listand agoal. The player has a list ofheld-cards, initially empty. The player
makes a move by eitherdrawing, which means removing the first card in the card-list from the card-list and
adding it to the held-cards, ordiscarding, which means choosing one of the held-cards to remove. The game
ends either when the player chooses to make no more moves or when the sum of the values of the held-cards
is greater than the goal.
The objective is to end the game with a low score (0 is best). Scoring works as follows: Letsumbe the sum
of the values of the held-cards. Ifsumis greater thangoal, thepreliminary scoreis three times (sum−goal),
else the preliminary score is (goal−sum). The score is the preliminary score unless all the held-cards are
the same color, in which case the score is the preliminary score divided by 2 (and rounded down as usual
with integer division; use ML’sdivoperator).


(a) Write a functioncard_color, which takes a card and returns its color (spades and clubs are black,
diamonds and hearts are red). Note: One case-expression is enough.


(b) Write a function card_value, which takes a card and returns its value (numbered cards have their
number as the value, aces are 11, everything else is 10). Note: One case-expression is enough.


(c) Write a functionremove_card, which takes a list of cardscs, a cardc, and an exceptione. It returns a
list that has all the elements ofcsexceptc. Ifcis in the list more than once, remove only the first one.
Ifcis not in the list, raise the exceptione. You can compare cards with=.

(d) Write a functionall_same_color, which takes a list of cards and returns true if all the cards in the
list are the same color. Hint: An elegant solution is very similar to one of the functions using nested
pattern-matching in the lectures.


(e) Write a functionsum_cards, which takes a list of cards and returns the sum of their values.Use a locally
defined helper function that is tail recursive. (Take “calls use a constant amount of stack space” as a
requirement for this problem.)

(f) Write a functionscore, which takes acard list(the held-cards) and anint(the goal) and computes
the score as described above.

(g) Write a functionofficiate, which “runs a game.” It takes acard list(the card-list) amove list
(what the player “does” at each point), and anint(the goal) and returns the score at the end of the
game after processing (some or all of) the moves in the move list in order. Use a locally defined recursive
helper function that takes several arguments that together represent the current state of the game. As
described above:

- The game starts with the held-cards being the empty list.
- The game ends if there are no more moves. (The player chose to stop since themove listis empty.)
- If the player discards some cardc, play continues (i.e., make a recursive call) with the held-cards
    not havingcand the card-list unchanged. Ifcis not in the held-cards, raise theIllegalMove
    exception.
- If the player draws and the card-list is (already) empty, the game is over. Else if drawing causes
    the sum of the held-cards to exceed the goal, the game is over (after drawing). Else play continues
    with a larger held-cards and a smaller card-list.


Sample solution for (g) is under 20 lines.


3.Challenge Problems:

(a) Writescore_challengeandofficiate_challengeto be like their non-challenge counterparts except
each ace can have a value of 1 or 11 andscore_challengeshould always return the least (i.e., best)
possible score. (Note the game-ends-if-sum-exceeds-goal rule should apply only if there is no sum that
is less than or equal to the goal.) Hint: This is easier than you might think.

(b) Writecareful_player, which takes a card-list and a goal and returns a move-list such that calling
officiatewith the card-list, the goal, and the move-list has this behavior:

- The value of the held cards never exceeds the goal.
- A card is drawn whenever the goal is more than 10 greater than the value of the held cards. As a
    detail, you should (attempt to) draw, even if no cards remain in the card-list.
- If a score of 0 is reached, there must be no more moves.
- If it is possible to reach a score of 0 by discarding a card followed by drawing a card, then this
    must be done. Notecareful_playerwill have to look ahead to the next card, which in many card
    games is considered “cheating.” Also note that the previous requirement takes precedence: There
    must be no more moves after a score of 0 is reached even if there is another way to get back to 0.

Notes:

- There may be more than one result that meets the requirements above. The autograder should
    work for any correct strategy — it checks that the result meets the requirements.
- This problem isnota continuation of problem 3(a). In this problem, all aces have a value of 11.


Type Summary

Evaluating a correct homework solution should generate these bindings, in addition to the bindings from the
code provided to you —but see the important caveat that follows.

val all_except_option = fn : string * string list -> string list option
val get_substitutions1 = fn : string list list * string -> string list
val get_substitutions2 = fn : string list list * string -> string list
val similar_names = fn : string list list * {first:string, last:string, middle:string}
-> {first:string, last:string, middle:string} list
val card_color = fn : card -> color
val card_value = fn : card -> int
val remove_card = fn : card list * card * exn -> card list
val all_same_color = fn : card list -> bool
val sum_cards = fn : card list -> int
val score = fn : card list * int -> int
val officiate = fn : card list * move list * int -> int

Important Caveat

The REPL may give your functionsequivalent typesormore generaltypes. This is fine. In the sample
solution, the bindings for problems 1d, 2a, 2b, 2c, and 2d were all more general. For example,card_color
had typesuit * ’a -> colorandremove_cardhad type’’a list * ’’a * exn -> ’’a list. They
are more general, which means there is a way to replace thetype variables(’aor’’a) with types to get the
bindings listed above. As for equivalent types, becausetype card = suit*rank, types likecard -> int
andsuit*rank->intare equivalent. They are the same type, and the REPL simply chooses one way of
printing the type. Also, the order of fields in records never matters.

If you write down explicit argument types for functions, you will probably not see equivalent or more-general
types, but we encourage the common ML approach of omitting all explicit types.

Of course, generating these bindings does not guarantee that your solutions are correct.Test your functions:
Put your testing code in a second file. We will not grade the testing file, nor will you turn it in, but surely
you want to run your functions and record your test inputs in a file.

Assessment

We will automatically test your functions on a variety of inputs, including edge cases. We will also ask peers
to evaluate your code for simplicity, conciseness, elegance, and good formatting including indentation and
line breaks. Your solution will also be checked for using only features discussed so far in class, and to ensure
that it does not use any of the banned features listed at the beginning of the assignment, such ashdand
#foo.

Turn-in Instructions

First, follow the instructions on the course website to submit your solution file (not your testing file) for auto-
grading. Do not proceed to the peer-assessment submission until you receive a high-enough grade from the
auto-grader: Doing peer assessment requires instructions that include a sample solution, so these instructions
will be “locked” until you receive high-enough auto-grader score. Then submit your same solution file again
for peer assessment and follow the peer-assessment instructions.
