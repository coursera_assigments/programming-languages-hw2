(* Dan Grossman, Coursera PL, HW2 Provided Code *)

(* if you use this function to compare two strings (returns true if the same
   string), then you avoid several of the functions in problem 1 having
   polymorphic types that may be confusing *)
fun same_string(s1 : string, s2 : string) =
    s1 = s2

(* put your solutions for problem 1 here *)

fun all_except_option (s1 : string , s2 :string list) = 
    case s2 of 
        [] => NONE
        | x::xs => if same_string(s1,x) then SOME(xs) else 
            case all_except_option(s1,xs) of 
                NONE => NONE
                | SOME(xs) => SOME(x::xs)

fun get_substitutions1 (slist : string list list, s1 : string)=
    case slist of 
        [] =>  []
        | x::xs => 
         case all_except_option(s1, x) of 
            NONE => get_substitutions1(xs, s1)
            | SOME y => y @ get_substitutions1(xs, s1)
         


fun get_substitutions2 (slist : string list list, s1 : string)=
    let 
        fun aux (slist, s1, result) = 
            case slist of 
                [] => result
                | x::xs => 
                    case all_except_option(s1, x) of 
                        NONE => aux(xs, s1, result)
                        | SOME y => aux(xs, s1, y @ result)
    in
        aux(slist, s1, [])
    end


fun similar_names (xs : string list list, name : {
    first : string, 
    middle:string,
    last:string
    }) = 
    let 
        val {first= first , middle  =middle, last = last} = name;
        fun aux (names, result) = 
            case names of 
                [] => result
                | name::names' => aux(names',{first =  name , middle = middle, last = last } :: result)
    in
      aux(get_substitutions1(xs, first), [name]) 
    end

(* you may assume that Num is always used with values 2, 3, ..., 10
   though it will not really come up *)

datatype suit = Clubs | Diamonds | Hearts | Spades

datatype rank = Jack | Queen | King | Ace | Num of int 
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw 

exception IllegalMove

(* put your solutions for problem 2 here *)

fun card_color (card : card) =  
    case card of 
        (suit, rank) => if suit = Spades orelse suit = Clubs
                        then Black
                        else Red

fun card_value (card : card) =
    case card of 
          (_, Num n) => n
        | (_, Ace) => 11
        | _        => 10

fun remove_card (cs : card list , c : card , e : exn ) = 
    case cs of 
        [] => raise e
        | x::cs' => if x = c then cs' else remove_card(cs' , c, e)

fun all_same_color (cs : card list) = 
    case cs of 
        [] => true
        |c::[] => true
        |c1::c2::cs' => card_color(c1) = card_color c2 andalso all_same_color(c2::cs')

fun sum_cards (cs : card list) =
    let 
        fun aux (cs, acc) = 
            case cs of 
                [] => acc 
                | c::cs' => aux(cs', acc + card_value(c))
    in
        aux(cs, 0)
    end

fun score (held_cards : card list, goal : int) =
    let 
        val sum = sum_cards(held_cards) 
    in
        if sum > goal 
        then 
            if all_same_color(held_cards)
            then 3 * (sum - goal) div 2
            else 3 * (sum - goal)
        else 
            if all_same_color(held_cards)
            then (goal - sum) div 2
            else  (goal - sum)
           
    end

fun officiate (card_list : card list, move_list : move list, goal : int) = 
    let 
        fun aux (card_list, move_list, held_cards) =
            let 
                val sum = sum_cards(held_cards)
            in
                if sum > goal then score(held_cards, goal)
                else 
                    case move_list of 
                        [] => score(held_cards, goal)
                        |Discard c::move_list' => aux(card_list, move_list', remove_card(held_cards, c, IllegalMove))
                        | Draw::move_list' => case card_list of 
                                                [] => score(held_cards, goal)
                                                |c::card_list'  => aux(card_list', move_list', c::held_cards )

            end 
    in
        aux(card_list, move_list, [])
    end